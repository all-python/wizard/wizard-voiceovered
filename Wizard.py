import random
import pyttsx3
tts = pyttsx3.init()
tts.setProperty('voice', 'ru')
tts.runAndWait()
response = ["Звезды говорят, что это возможно.", "Шепоты сообщают, что это маловероятно.", "6 чувство склоняется к отрицательному ответу.", "У карт хорошее предчувствие на этот счет.", "У зеркал нет точной версии событий.", "Стеклянный шар в замешательстве по поводу грядущего."]
question = ["Какой вопрос заставил вас заглянуть за грань времени?", "Гадалка готова услышать ваш вопрос.", "Назовите вопрос, терзающий ваш разум."]
ball = ["🧞", "🧚", "🎲", "🎱", "✨", "🧙", "🌚"]
game = True
while game is True:
    a = random.randint(0, 2)
    b = random.randint(0, 5)
    print(f"_/({ball[random.randint(0, 6)]})\_")
    print(question[a])
    tts.say(question[a])
    tts.runAndWait()
    quest = input()
    tts.say("Вы спросили"+quest)
    print("Ответ гадалки:")
    tts.say("Ответ гадалки:")
    tts.runAndWait()
    if quest=="выход":
        game = False
    print(response[b])
    tts.say(response[b])
    tts.runAndWait()
    print("-"*50)